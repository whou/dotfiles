# General bash configuration

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

platform=""

case "$OSTYPE" in
    linux*)   platform="$HOME/.linux.sh"   ;;
    msys*)    platform="$HOME/.windows.sh" ;;
esac

# source the platform specific script if it is present
if [ -f "$platform" ]; then
    . "$platform"
fi

alias ls='ls --color=auto'
alias yt-dl='youtube-dl'
alias spiceup='spicetify upgrade && spicetify restore backup apply'

alias gs='git status'
alias gl='git log --oneline'
alias ga='git add'
alias gaa='git add --all'
alias gc='git commit'
alias gcm='git commit -m'
alias gp='git push'

# environment variables i use in every system i use
export DOTNET_CLI_TELEMETRY_OPTOUT=1
export CMAKE_GENERATOR=Ninja
export GOPROXY=direct
export PATH=$PATH:~/.local/bin

complete -cf sudo
complete -cf man

git-clone () {
    if [ "$2" = "-ssh" -o "$2" = "--ssh" ]; then
        git clone git@$1:$3 ${@:4}
    else
        git clone https://$1/$2 ${@:3}
    fi
}

gh-clone () {
    git-clone github.com $@
}

gl-clone () {
    git-clone gitlab.com $@
}

ht-clone () {
    git-clone git.sr.ht $@
}

cb-clone () {
    git-clone codeberg.org $@
}

get-license () {
    curl "https://www.gnu.org/licenses/$1" > COPYING
}

get-gpl () {
    get-license gpl-3.0.txt
}

get-agpl () {
    get-license agpl-3.0.txt
}

get-lgpl () {
    get-license lgpl-3.0.txt
}

search-history () {
    # if we don't have ripgrep installed, use grep
    if ! [ -x "$(command -v rg)" ]; then
        cat ~/.bash_history | grep -a $1
    else
        cat ~/.bash_history | rg $1
    fi
}

extract () {
     if [ -f $1 ] ; then
         case $1 in
             *.tar.bz2)   tar xjf $1         ;;
             *.tar.gz)    tar xzf $1         ;;
             *.tar.xz)    tar xf $1          ;;
             *.tar.zst)   tar --zstd -xf $1  ;;
             *.bz2)       bunzip2 $1         ;;
             *.rar)       rar x $1           ;;
             *.gz)        gunzip $1          ;;
             *.xz)        unxz $1            ;;
             *.tar)       tar xf $1          ;;
             *.tbz2)      tar xjf $1         ;;
             *.tgz)       tar xzf $1         ;;
             *.zip)       unzip $1           ;;
             *.Z)         uncompress $1      ;;
             *.7z)        7z x $1            ;;
             *.zst)       zstd -d $1         ;;
             *)           echo "'$1' cannot be extracted via extract()" ;;
         esac
     else
         echo "'$1' is not a valid file"
     fi
}

# Linux specific variables/functions

clipcat () {
    # check if xclip is installed
    if hash xclip 2>/dev/null; then
        cat $1 | xclip -selection clipboard
    else
        echo "Hey man, you might wanna install xclip to use this command. :)"
    fi
}

if hash fzf 2>/dev/null; then
    source /usr/share/doc/fzf/examples/key-bindings.zsh
fi

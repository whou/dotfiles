# Windows specific variables/functions

alias gradlew='TERM=cygwin ./gradlew $@'
alias mpv='mpv.com'
alias sudo='gsudo'
alias python3='py'
alias msbuild='msbuild.exe'

clipcat () {
    clip < $1
}

local wezterm = require('wezterm')

local config = wezterm.config_builder()

config.font = wezterm.font('JetBrainsMono Nerd Font')
config.color_scheme = 'Dracula (Official)'

config.enable_tab_bar = false
config.audible_bell = 'Disabled'

config.window_close_confirmation = 'NeverPrompt'
config.window_background_opacity = 0.8
config.window_padding = {
	left = 0,
	right = 0,
	top = 0,
	bottom = 0,
}

return config

SRC = .bash.sh .zsh.sh
SCRIPTS = video-dl audio-dl secrecy md5cmp shacmp
SCRIPTS_DIR = scripts

BASHRC_OUTPUT := /dev/null
# if there isn't a call to .bash.sh in .bashrc
ifeq (,$(shell grep ". ~/.bash.sh" ~/.bashrc))
	BASHRC_OUTPUT = ~/.bashrc
endif

ZSHRC_OUTPUT := /dev/null
# if there isn't a call to .zsh.sh in .zshrc
ifeq (,$(shell grep ". ~/.zsh.sh" ~/.zshrc))
	ZSHRC_OUTPUT = ~/.zshrc
endif

.PHONY: install windows root

# $(call copy_files_to,src_dir,file_list,dest_dir)
define copy_files
	$(foreach file,$(2),cp $(1)/$(file) $(3)/$(file);)
endef

# $(call install_rc,platform)
define install_rc
	cat .bashrc >> $(BASHRC_OUTPUT)
	cat .zshrc >> $(ZSHRC_OUTPUT)
endef

install:
	$(call copy_files,.,$(SRC),~)
	$(call install_rc)
	cp .linux.sh ~/
	cp .linux.zsh ~/
	mkdir -p ~/.config/alacritty
	mkdir -p ~/.local/bin
	mkdir -p ~/.config/nvim
	cp alacritty.toml ~/.config/alacritty/
	cp .wezterm.lua ~/
	$(call copy_files,$(SCRIPTS_DIR),$(SCRIPTS),~/.local/bin)
	cp -r nvim/* ~/.config/nvim/
	cp -r nvim/.[^.]* ~/.config/nvim/

windows:
	$(call copy_files,.,$(SRC),~)
	$(call install_rc)
	cp .windows.sh ~/
	mkdir -p ~/AppData/Roaming/alacritty
	mkdir -p ~/.config/alacritty/
	mkdir -p ~/.local/bin
	$(call copy_files,$(SCRIPTS_DIR),$(SCRIPTS),~/.local/bin)
	cp windows.alacritty.toml ~/AppData/Roaming/alacritty/alacritty.toml
	cp windows.alacritty.toml ~/.config/alacritty/alacritty.toml
	cp .wezterm.lua ~/.wezterm.lua

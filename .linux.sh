# Linux specific variables/functions

clipcat () {
    # check if xclip is installed
    if hash xclip 2>/dev/null; then
        cat $1 | xclip -selection clipboard
    else
        echo "Hey man, you might wanna install xclip to use this command. :)"
    fi
}

# PS1 color config:
#
# \[\033[COLORm\]
#
# the color numbers being:
# black: 30
# blue: 34
# cyan: 36
# green: 32
# purple: 35
# red: 31
# white: 37
# yellow: 33
# clear color: 00

BRACKET_COLOR=$'\033[31m'
USER_COLOR=$'\033[33m'
HOST_COLOR=$'\033[36m'
PWD_COLOR=$'\033[32m'
GIT_COLOR=$'\033[34m'
OTHERS_COLOR=$'\033[00m'
CMD_SIGN='$'

# if current user is root
if [[ $EUID -eq 0 ]]; then
    USER_COLOR=$'\033[31m'
    CMD_SIGN='#'
fi

# [user@host pwd (branch)]$
PS1='\[${BRACKET_COLOR}\][\[${USER_COLOR}\]\u\[${OTHERS_COLOR}\]@\[${HOST_COLOR}\]\h \[${PWD_COLOR}\]\W\[${GIT_COLOR}\]$(__git_ps1 " (%s)")\[${OTHERS_COLOR}\]\[${BRACKET_COLOR}\]]\[${OTHERS_COLOR}\]${CMD_SIGN} '

if hash fzf 2>/dev/null; then
    source /usr/share/doc/fzf/examples/key-bindings.bash
fi

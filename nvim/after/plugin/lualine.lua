require('lualine').setup {
  options = {
    icons_enabled = true,
    theme = 'iceberg_dark'
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'diagnostics'}
  }
}

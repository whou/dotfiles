vim.keymap.set('n', '<leader>dl', ':call vimspector#Launch()<CR>')
vim.keymap.set('n', '<leader>ds', ':call vimspector#Reset()<CR>')
vim.keymap.set('n', '<leader>dr', ':call vimspector#Restart()<CR>')
vim.keymap.set('n', '<leader>dc', ':call vimspector#Continue()<CR>')
vim.keymap.set('n', '<leader>db', ':call vimspector#ToggleBreakpoint()<CR>')
vim.keymap.set('n', '<leader>dx', ':call vimspector#ClearBreakpoints()<CR>')

-- navigate the call stack
vim.keymap.set('n', '<leader>dk', ':call vimspector#UpFrame()<CR>')
vim.keymap.set('n', '<leader>dj', ':call vimspector#DownFrame()<CR>')

-- inspect expression under cursor
vim.keymap.set('n', '<leader>di', '<Plug>VimspectorBalloonEval')
vim.keymap.set('x', '<leader>di', '<Plug>VimspectorBalloonEval')

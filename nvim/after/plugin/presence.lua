local presence = require('presence')

vim.api.nvim_create_user_command('PresenceStatus', function()
  print('Neovim is connected to Discord: ' .. tostring(presence.is_connected))
end, {})

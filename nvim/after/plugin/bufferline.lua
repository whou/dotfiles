local error_char = ''
local warning_char = ''
local info_char = ''

require('bufferline').setup {
  options = {
    mode = 'tabs',
    separator_style = 'slant',
    diagnostics = 'nvim_lsp',
    diagnostics_indicator = function(count, level, diagnostics_dict, context)
      local str = ' '

      for err, num in pairs(diagnostics_dict) do
        local icon = (function()
          if err == 'error' then
            return error_char
          elseif err == 'warning' then
            return warning_char
          else
            return info_char
          end
        end)()

        str = str .. '|' .. icon .. ' ' .. num
      end

      return str
    end,
    hover = {
      enabled = true,
      delay = 10,
      reveal = {'close'}
    }
  }
}

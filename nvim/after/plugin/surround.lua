local v_chars = {'(', ')', '{', '}', '[', ']', '\'', '"'}
for _, char in pairs(v_chars) do
  vim.keymap.set('v', char, '<Plug>(nvim-surround-visual)'..char)
end


local lsp_zero = require('lsp-zero')

lsp_zero.on_attach(function(client, bufnr)
  -- see :help lsp-zero-keybindings
  -- to learn the available actions
  lsp_zero.default_keymaps({ buffer = bufnr })

  vim.lsp.inlay_hint.enable(true)

  if client.server_capabilities.signatureHelpProvider then
    require('lsp-overloads').setup(client, {})

    vim.keymap.set('n', '<C-Space>', '<cmd>LspOverloadsSignature<CR>')
    vim.keymap.set('i', '<C-Space>', '<cmd>LspOverloadsSignature<CR>')
  end
end)

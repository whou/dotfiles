local telescope = require('telescope')
local builtin = require('telescope.builtin')

vim.keymap.set('n', '<leader>ff', builtin.find_files, {})
vim.keymap.set('n', '<leader>fp', builtin.git_files, {})
vim.keymap.set('n', '<leader>fg', builtin.live_grep, {})
vim.keymap.set('n', '<leader>fb', builtin.buffers, {})
vim.keymap.set('n', '<leader>fh', builtin.help_tags, {})

telescope.setup {
  pickers = {
    find_files = {
      find_command = { 'rg', '--files', '--hidden', '--glob', '!**/.git/*' }
    }
  },
  extensions = {
    ['ui-select'] = {
      require('telescope.themes').get_dropdown {}
    },
    ['media_files'] = {
      filetypes = {'png', 'svg', 'jpg', 'jpeg', 'webp', 'mp4', 'webm', 'pdf', 'ttf', 'otf'},
      find_cmd = 'rg'
    }
  }
}

telescope.load_extension('ui-select')
telescope.load_extension('media_files')

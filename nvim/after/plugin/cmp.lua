local cmp = require('cmp')
-- local cmp_action = require('lsp-zero').cmp_action()

cmp.setup({
  sources = {
    {name = 'nvim_lsp'},
    {name = 'luasnip'}
  },
  mapping = cmp.mapping.preset.insert({
    -- `Tab` key to confirm completion
    ['<Tab>'] = cmp.mapping.confirm({select = false}),

    -- `Ctrl+Space` to trigger completion menu
    ['<C-Space>'] = cmp.mapping.complete(),

    -- navigate between snippet placeholder
    -- ['C-f'] = cmp_action.luasnip_jump_forward(),
    -- ['C-b'] = cmp_action.luasnip_jump_backward(),

    -- scroll up and down in the completion documentation
    ['<C-b>'] = cmp.mapping.scroll_docs(-4);
    ['<C-f>'] = cmp.mapping.scroll_docs(4);

    -- scroll up and down in the completion menu
    ['<C-n>'] = cmp.mapping.select_next_item(),
    ['<C-p>'] = cmp.mapping.select_prev_item(),
  }),
  preselect = 'item',
  completion = {
    completeopt = 'menu,menuone,noinsert'
  }
})

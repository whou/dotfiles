local ls = require('luasnip')

local t = ls.text_node
local i = ls.insert_node
local rep = require('luasnip.extras').rep
local fmt = require('luasnip.extras.fmt').fmt

ls.add_snippets('c', {
  ls.snippet(' ifdefheader', fmt(
  [[
  ifndef {}
  #define {}

  {}

  #endif // {}
  ]], {
    i(1, 'HEADER_H'), rep(1), i(0), rep(1)
  }))
})
ls.filetype_extend('cpp', {'c'})

-- keybinds for browsing snippets
vim.keymap.set({ 'i', 's' }, '<A-k>', function()
  if ls.expand_or_jumpable() then
    ls.expand_or_jump()
  end
end, { silent = true })

vim.keymap.set({ 'i', 's' }, '<A-j>', function()
  if ls.jumpable(-1) then
    ls.jump(-1)
  end
end, { silent = true })

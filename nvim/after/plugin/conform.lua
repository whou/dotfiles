local conform = require('conform')

conform.setup({
  formatters_by_ft = {
    c = { 'clang_format' },
    cpp = { 'clang_format' },
    go = { 'goimports', 'gofmt' },
  },
  format_on_save = function(bufnr)
    local ignore_filetypes = {}
    if vim.tbl_contains(ignore_filetypes, vim.bo[bufnr].filetype) then
      return
    end

    if vim.g.disable_autoformat or vim.b[bufnr].disable_autoformat then
      return
    end

    local ignore_paths = { '/node_modules/', '/build/' }
    local bufname = vim.api.nvim_buf_get_name(bufnr)
    for _, path in ipairs(ignore_paths) do
      if bufname:match(path) then
        return
      end
    end

    return { timeout_ms = 500, lsp_format = 'fallback' }
  end,
  format_after_save = function(bufnr)
    if vim.g.disable_autoformat or vim.b[bufnr].disable_autoformat then
      return
    end

    return { lsp_format = 'fallback' }
  end
})

vim.api.nvim_create_user_command('FormatDisable', function(args)
  if args.bang then
    -- using FormatDisable! will disable just for this buffer
    vim.b.disable_autoformat = true
  else
    vim.g.disable_autoformat = true
  end
end, { desc = 'Disable autoformat on save', bang = true })

vim.api.nvim_create_user_command('FormatEnable', function()
  vim.b.disable_autoformat = false
  vim.g.disable_autoformat = false
end, { desc = 'Enable autoformat on save' })

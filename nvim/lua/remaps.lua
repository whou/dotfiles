-- go to definition in new tab
vim.keymap.set('n', '<C-d>', '<C-w><C-]><C-w>T')
-- open code actions (quickfix) menu
vim.keymap.set('n', '<C-.>', vim.lsp.buf.code_action)

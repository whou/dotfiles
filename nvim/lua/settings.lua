vim.g.mapleader = ' '
vim.g.maplocalleader = '\\'

vim.opt.nu = true

vim.opt.tabstop = 4
vim.opt.shiftwidth = 0
vim.opt.softtabstop = 0

vim.opt.hlsearch = false
vim.opt.mousemoveevent = true

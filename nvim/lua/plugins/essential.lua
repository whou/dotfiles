return {
  -- theme
  {
    'folke/tokyonight.nvim',
    lazy = false,
    priority = 100,
    config = function()
      vim.cmd.colorscheme('tokyonight-night')
    end
  },

  -- dependecies stuff
  'nvim-tree/nvim-web-devicons',
  'roxma/vim-hug-neovim-rpc',
  'roxma/nvim-yarp',
}

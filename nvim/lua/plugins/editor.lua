return {
  {'kylechui/nvim-surround', version = '*', event = 'VeryLazy', config = true},
  'nvim-lualine/lualine.nvim',
  'gelguy/wilder.nvim',
  {
    'nvim-treesitter/nvim-treesitter',
    build = function()
      local ts_update = require('nvim-treesitter.install').update({ with_sync = true})
      ts_update()
    end
  },
  {'nvim-telescope/telescope.nvim', version = '0.1.6', dependencies = {'nvim-lua/plenary.nvim'}},
  'nvim-telescope/telescope-ui-select.nvim',
  {'nvim-telescope/telescope-media-files.nvim', dependencies = {'nvim-lua/popup.nvim'}},
  {'akinsho/toggleterm.nvim', config = true},
  'akinsho/bufferline.nvim',
  'puremourning/vimspector',
}

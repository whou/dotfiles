return {
  {'m4xshen/autoclose.nvim', config = true},
  {'numToStr/Comment.nvim', lazy = false, config = true},
  'edluffy/hologram.nvim',
  'pwnxpl0it/presence.nvim',
  {'lewis6991/gitsigns.nvim', config = true},
  {'saecki/crates.nvim', tag = 'stable', config = true},
  {
    'michaelrommel/nvim-silicon', lazy = true, cmd = 'Silicon', config = function()
      require('silicon').setup {
        to_clipboard = true,
        font = 'JetBrainsMono Nerd Font Mono=34;Noto Color Emoji=34'
      }
    end
  }
}

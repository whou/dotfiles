return {
  {'williamboman/mason.nvim', config = true},
  'williamboman/mason-lspconfig.nvim',
  {'VonHeikemen/lsp-zero.nvim', branch = 'v3.x'},
  'neovim/nvim-lspconfig',
  'Issafalcon/lsp-overloads.nvim',
  'hrsh7th/cmp-nvim-lsp',
  'hrsh7th/nvim-cmp',
  'L3MON4D3/LuaSnip',
  'saadparwaiz1/cmp_luasnip',
  {'stevearc/conform.nvim', event = {'BufWritePre'}, cmd = {'ConformInfo'}},
  {'j-hui/fidget.nvim', config = true},
  {'akinsho/flutter-tools.nvim', lazy = false, config = true},
  {'folke/neodev.nvim', config = true}
}
